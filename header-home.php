<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>
	<?php if(ot_get_option('layout-header') == 'header-1'): ?>
	<script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 70) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'block'});
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'none'});
			}
		});
	</script>
	<?php endif; ?>

</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home clear" role="banner">
		<!-- Wrapper for Slides -->
		<?php if(ot_get_option('layout-header') == 'header-1'): ?>
			<div class="sub-header-1">
				<!-- logo -->
				<div class="wrap-logo">
					<div class="logo">
						<?php krs_headlogo(); ?>
						<?php if (ot_get_option('krs_head_hotelinfo_actived') == 'on'): ?>
							<div class="header-address">
								<span> <i class="fa fa-map-marker"></i> <?php echo ot_get_option('krs_address'); ?></span>
								<span> <i class="fa fa-phone"></i> <?php echo ot_get_option('krs_phone'); ?></span>
								<span><i class="fa fa-envelope"></i> <a href="mailto:<?php echo ot_get_option('krs_email'); ?>"><?php echo ot_get_option('krs_email'); ?></a></span>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<!-- /logo -->

				<!-- nav -->
				<nav id="mainNav" class="navbar navbar-custom">
					<div class="navbar-brand"><?php krs_headlogo(); ?></div>
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<?php karisma_nav(); ?>
							<div class="ff">
								<?php if ( ! dynamic_sidebar( 'box-language' ) ) : ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</nav>
				<!-- /nav -->

			</div>
		<?php else: ?>
			<div class="sub-header-2 navbar-fixed-top">
				<!-- logo -->
				<div class="container clearfix">
					<div class="top-logo pull-left">
						<?php krs_headlogo(); ?>
					</div>
					<?php if (ot_get_option('krs_head_hotelinfo_actived') == 'on'): ?>
					<div class="header-address pull-right">
						<span> <i class="fa fa-map-marker"></i> <?php echo ot_get_option('krs_address'); ?></span>
						<span> <i class="fa fa-phone"></i> <?php echo ot_get_option('krs_phone'); ?></span>
						<span><i class="fa fa-envelope"></i> <a href="mailto:<?php echo ot_get_option('krs_email'); ?>"><?php echo ot_get_option('krs_email'); ?></a></span>
					</div>
					<?php endif; ?>
				</div>
				<!-- /logo -->

				<!-- nav -->
				<nav id="mainNav" class="navbar navbar-custom-2">
					<div class="navbar-brand"><?php krs_headlogo(); ?></div>
					<div class="container">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
							</button>
						</div>
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<?php karisma_nav(); ?>
						</div>
					</div>
				</nav>
				<!-- /nav -->

			</div>
		<?php endif; ?>
		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image:linear-gradient(rgba(0, 0, 0, 0.40),rgba(0, 0, 0, 0.10)),url('<?php echo $thumb_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>

			<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev"><span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next"><span class="icon-next"></span></a>
		</div>
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=-1;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == -1) {
								$active = 'active';
							} else {$active = '';}
							?>
							<li data-target="#homeSliderCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>

						</div>
						<?php
						}
					}
				}
			}
			?>
		</ol>
	<!-- Controls -->

	<div class="booking-square"></div>
	<div class="booking-slide"></div>
	<div class="booking-box">
		<?php do_shortcode("[booking_engine]")?>
		</div>
</header>
	<!-- /header -->
