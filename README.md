=== Indohotels Red Child ===
Contributors: Amri Karisma
Donate link: https://www.amrikarisma.com/
Tags: simple, seo
Requires at least: 4.8
Stable tag: v1.0.2
Version: 1.0.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Indohotels Gold Child is a child theme of Indohotelswp


== Installation ==

This section describes how to install the child theme and get it working.

1. You need to install and activate the parent theme first, indohotelswp
2. In your admin panel, go to Appearance -> Themes and click the 'Add New' button.
3. Click on the 'Activate' button to use your new theme right away.
4. Navigate to Appearance > Customize in your admin panel and customize to taste.


== Changelog ==

= 1.0.2 =
* add news template

= 1.0.1 =
* Adjust booking form
