<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<?php wp_head(); ?>
	<?php if(ot_get_option('layout-header') == 'header-1'): ?>
	<script type="text/javascript">
		jQuery(window).scroll(function(){
			if (jQuery(this).scrollTop() > 70) {
				jQuery('#mainNav').addClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'block'});
			} else {
				jQuery('#mainNav').removeClass('navbar-fixed-top');
				jQuery('.navbar-brand').css({'display':'none'});
			}
		});
	</script>
	<?php endif; ?>
</head>
<body id="modalParrent" <?php body_class(); ?>>
	<header class="header-all clear" <?php krs_header_cover(); ?> role="banner">
		<!-- logo -->
		<div class="wrap-logo">
			<div class="logo">
				<?php krs_headlogo(); ?>
				<?php if (ot_get_option('krs_head_hotelinfo_actived') == 'on'): ?>
					<div class="header-address">
						<span> <i class="fa fa-map-marker"></i> <?php echo ot_get_option('krs_address'); ?></span>
						<span> <i class="fa fa-phone"></i> <?php echo ot_get_option('krs_phone'); ?></span>
						<span><i class="fa fa-envelope"></i> <a href="mailto:<?php echo ot_get_option('krs_email'); ?>"><?php echo ot_get_option('krs_email'); ?></a></span>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<!-- /logo -->
		<!-- <div class="shadow-box">
		</div> -->
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom">
			<div class="navbar-brand"><?php //krs_headlogo(); ?></div>
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
					<div class="ff">
						<?php if ( ! dynamic_sidebar( 'box-language' ) ) : ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</nav>
		<!-- /nav -->
	</header>
	<!-- /header -->
