<?php get_header('image'); ?>

<main role="main" class="col-md-12">
<div class="container">
	<!-- section -->
	<section class="box-content">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>">
				<div class="room-details">
					<div class="col-sm-12 col-md-12">
						<div class="room-box">
							<div class="room-title-box text-center">
								<h2 class="room-title"><?php the_title(); ?></h2>
							</div>
							<!-- Slider -->
							<div id="slider">
								<div id="carousel-bounding-box">
									<div id="myCarousel" class="carousel slide">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<?php
											$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
											if ( !empty( $images ) ) {
												$i = 0;
												foreach ( $images as $image ) {
													if($i++ == 0) {
														$active = 'active';
													} else {$active = '';}
													echo '<div id="slide-'. $i .'" class="'. $active .' item">';

													echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
													echo '<a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>';
													echo '</div>';
												}
											}
											?>
										</div><!-- end .carousel-inner -->
									</div><!-- end #myCarousel -->
								</div><!-- end #carousel-bounding-box -->
							</div><!--/Slider-->

							<div class="room-details-spec">
								<div class="row">
									<div class="col-md-3 col-sm-3 col-xs-12 spec">
										<span class="room-title">Room Size</span>
										<span class="room-value"><?php echo rwmb_meta( 'room_size' ); ?> m2</span>
									</div><!-- end .col-md-3 -->
									<div class="col-md-3 col-sm-3 col-xs-12 spec">
										<span class="room-title">View </span>
										<span class="room-value"><?php echo rwmb_meta( 'room_view' ); ?></span>
									</div><!-- end .col-md-3 -->
									<div class="col-md-3 col-sm-3 col-xs-12 spec">
										<span class="room-title">Ocupancy</span>
										<span class="room-value"><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
									</div><!-- end .col-md-3 -->
									<div class="col-md-3 col-sm-3 col-xs-12 spec">
										<span class="room-title">Bed Size</span>
										<span class="room-value"><?php echo rwmb_meta( 'bed_size' ); ?></span>
									</div><!-- end .col-md-3 -->
								</div><!-- end .row -->
							</div><!-- end .room-details-spec -->
						</div><!-- end .room-box -->

						<div class="room-details-desc">
							<?php the_content(); ?>
						</div>


						<?php /*
						<?php if(rwmb_meta( 'room_convenience' ) || rwmb_meta( 'room_indulgence' ) || rwmb_meta( 'room_comfort' )): ?>
						<h3 class="room-details-f-title">
							<?php _e('Room features', karisma_text_domain); ?>
						</h3>
						<div class="feature-box col-md-12">

							<?php if(!empty(rwmb_meta( 'room_convenience' ))): ?>
							<div class="col-md-4">
								<h4>
									<?php _e('For Your Convenience', karisma_text_domain); ?>
								</h4>
								<ul class="room-features">
									<?php
										$values = rwmb_meta( 'room_convenience' );
										foreach ( $values as $value )
										{
											echo '<li>'. $value . '</li>';
										}
										?>

								</ul>
							</div>
							<?php endif; ?>
							<?php if(!empty(rwmb_meta( 'room_indulgence' ))): ?>
							<div class="col-md-4">
								<h4>
									<?php _e('For Your Indulgence', karisma_text_domain); ?>
								</h4>

								<ul class="room-features">
									<?php
										$values = rwmb_meta( 'room_indulgence' );
										foreach ( $values as $value )
										{
											echo '<li>'. $value . '</li>';
										}
										?>
								</ul>
							</div>
							<?php endif; ?>
							<?php if(!empty(rwmb_meta( 'room_comfort' ))): ?>

							<div class="col-md-4">
								<h4>
									<?php _e('For Your Comfort', karisma_text_domain); ?>
								</h4>

								<ul class="room-features">
									<?php
										$values = rwmb_meta( 'room_comfort' );
										foreach ( $values as $value )
										{
											echo '<li>'. $value . '</li>';
										}
										?>
								</ul>
							</div>
							<?php endif; ?>

						</div>
						<?php endif; ?>
						*/ ?>

						<?php
						$data['propery_id'] = get_option('idn_booking_engine.propery_id');
						?>
						<div class="text-center">
							<a href="//www.indohotels.id/website/property/<?php echo $data['propery_id']; ?>" class="btn btn-check"><?php _e('Check Availability', karisma_text_domain); ?>
							</a>
						</div><!-- end .text-center -->
					</div><!-- end .col-md-12 -->
				</div><!-- end .room-details -->
			</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</main>

	<?php get_footer(); ?>
