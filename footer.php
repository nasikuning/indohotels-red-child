	<footer class="footer">

		<?php if(ot_get_option('layout-footer') == 'footer-alpha'): ?>

			<div class="footer-center">
				<div class="container">
					<?php if(!empty(ot_get_option('krs_address'))) : ?>
					<h4 class="widget-title">ADDRESS</h4>
					<div class="footer-address">
						<i class="fas fa-map-marker-alt"></i>
						<p><?php echo ot_get_option('krs_address'); ?></p>
					</div><!-- end .footer-address -->
					<?php endif; ?>

					<?php if(!empty(ot_get_option('krs_phone'))) : ?>
						<div class="footer-phone">
							<i class="fas fa-phone"></i>
							<p><?php echo ot_get_option('krs_phone'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(!empty(ot_get_option('krs_whatsapp'))) : ?>
						<div class="footer-whatsapp">
							<i class="fab fa-whatsapp"></i>
							<p><?php echo ot_get_option('krs_whatsapp'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(!empty(ot_get_option('krs_bbm'))) : ?>
						<div class="footer-bbm">
							<i class="fab fa-blackberry"></i>
							<p><?php echo ot_get_option('krs_bbm'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(!empty(ot_get_option('krs_fax'))) : ?>
						<div class="footer-phone">
							<i class="fas fa-fax"></i>
							<p><?php echo ot_get_option('krs_fax'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(!empty(ot_get_option('krs_email'))) : ?>
						<div class="footer-email">
							<i class="far fa-envelope-open"></i>
							<p>
								<a href="mailto:<?php echo ot_get_option('krs_email'); ?>">
									<?php echo ot_get_option('krs_email'); ?>
								</a>
							</p>
						</div><!-- end .footer-email -->
					<?php endif; ?>
				</div><!-- end .container -->
			</div><!-- end .footer-center -->

			<?php else: ?>

				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<?php if ( ! dynamic_sidebar( 'footer1' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="col-md-4">
							<?php if ( ! dynamic_sidebar( 'footer2' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="col-md-4">
							<?php if ( ! dynamic_sidebar( 'footer3' ) ) : ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

		<?php endif; ?>






		<?php krs_sn(); ?>
		<nav class="nav text-center nav-bottom">
			<?php karisma_nav_footer(); ?>
		</nav>

		<div class="footer-credits">
			<div class="container">
				<?php echo ot_get_option('krs_footcredits'); ?>
			</div>
		</div>
	</footer>
</div>
<!-- /wrapper -->
<?php wp_footer(); ?>


<?php if(!empty(ot_get_option('krs_map'))) : ?>
<a class="popup-gmaps" href="<?php echo ot_get_option('krs_map'); ?>">
	<span>
		<i class="fas fa-map-marker-alt"></i>
	</span>
</a>
<?php endif; ?>


</body>
</html>
