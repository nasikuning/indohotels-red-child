<?php 
/* Template Name: Available Rooms Page Template */ get_header(); ?>

<main role="main" class="col-md-12">
	<div class="container text-center"> <!-- container -->
		<!-- section -->
		<section>
			<h1 class="title text-center"><?php the_title(); ?></h1>
			<!-- Start Input available -->
			<div class="row">
				<div class='col-md-4'>
					<div class="form-group">
						<div class='input-group date' id='datetimepicker6'>
							<input type="text" class="form-control" id="from_date" readonly />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class='col-md-4'>
					<div class="form-group">
						<div class='input-group date' id='datetimepicker7'>
							<input type='text' class="form-control" id="to_date" placeholder="Check out Date" readonly />
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class='col-md-4'>
					<div class="form-group">
						<div class='input-group date' id='submit'>
							<input type='submit' class="btn-check" value="Check Available" />
						</div>
					</div>
				</div>
			</div>

			<!-- End Input available -->
		</section>
		<section>
			<?php 
			$args = array('post_type'=>'rooms');
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="col-md-6">
				<div class="room-thumb thumbnail">
					<!-- article -->
					<article id="post-<?php the_ID(); ?>" <?php post_class('rooms-post'); ?>>
						<!-- post thumbnail -->
						<div class="thumb">
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php the_post_thumbnail(array(300,150)); // Declare pixel size you need inside the array ?>
								</a>
							<?php endif; ?>
						</div>
						<!-- /post thumbnail -->
						<!-- post title -->
						<h2 class="title">
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
						</h2>
						<!-- /post title -->
						<div class="room-info">
							With Balcony <!-- Masih HTML ini -->
						</div>
						<div class="room-breakfast">
							Without Breakfast <!-- Masih HTML ini -->
						</div>
						<div class="room-price">
							IDR 750.000 / night <!-- Masih HTML ini -->
						</div>
						<div class="room-tax">including taxes</div>
						<div class="room-detail"><a href="#">see room details</a></div>
						<div class="rooom-no">Number of room(s)</div>
						<button class="book-room"><a href="<?php the_permalink(); ?>">Book Room</a></button>

					</article>
					<!-- /article -->
				</div>
			</div>

		<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>
			<h2><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h2>
		</article>
		<!-- /article -->

	<?php endif; ?>

	<?php get_template_part('pagination'); ?>

</section>
<!-- /section -->
</div> <!-- end container -->
</main>

<?php get_footer(); ?>