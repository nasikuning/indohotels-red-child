<?php

/* Template Name: News - Alpha Template */

get_header('image');

?>
<main role="main">
	<div class="hduni">
		<div class="container">
			<div class="row">

				<?php
					$args = array(
						'post_type' => 'post',
						'category_name' => 'news',
					);
					query_posts($args);
					if (have_posts()): while (have_posts()) : the_post();
				?>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="hdunibox hdunibox__wx margibo-30">
						<div class="hdunipic">
							<span>
								<?php if ( has_post_thumbnail()) :  ?>
									<?php the_post_thumbnail(array(300,150)); ?>
								<?php endif; ?>
							</span>
						</div>
						<div class="hduniname">
							<span class="metaname">by <?php the_author(); ?>, at <?php the_date(); ?></span>
							<h4><a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						</div><!-- end .hduniname -->
					</div><!-- end .hdunibox -->
				</div>

				<?php endwhile; ?>
				<?php else: ?>
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>
					</article>
				<?php endif; ?>
				<?php karisma_pagination(); ?>

			</div>
		</div>
	</div>

</main>

<?php get_footer(); ?>
