<?php /* Template Name: Contact Template */ get_header('image'); ?>


<main role="main">
	<!-- section -->
	<section class="container">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<h1 class="title text-center"><?php the_title(); ?></h1>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<br class="clear">

				<div class="row">
					<div class="col-md-6 col-sm-6">
						<?php the_content(); ?>
					</div><!-- end .col-md-6 col-sm-6 -->
					<div class="col-md-6 col-sm-6">
						<div class="contact-info">
							<?php if (!empty(rwmb_meta('contact_address'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fas fa-map-marker-alt" title="Address"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<?php echo rwmb_meta('contact_address'); ?>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_phone'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fas fa-phone" title="Phone"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_phone');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_mobile'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fas fa-mobile" title="Mobile"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_mobile');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_fax'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fas fa-fax" title="Fax"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_fax');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_email'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="far fa-envelope-open" title="Email"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_email');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_wa'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fab fa-whatsapp" title="Whatsapp"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_wa');
                      foreach ($values as $value) {
                        echo '<li><a href="https://api.whatsapp.com/send?phone='. $value .'">' . $value . '</a></li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_bbm'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fab fa-blackberry" title="Blackberry"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_bbm');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

							<?php if (!empty(rwmb_meta('contact_line'))) : ?>
							<div class="info-text">
								<div class="info-icon">
									<span class="icon-round">
										<i class="fab fa-line" title="Line"></i>
									</span>
								</div><!-- end .info-icon -->
								<div class="info-content">
									<ul>
										<?php
                      $values = rwmb_meta('contact_line');
                      foreach ($values as $value) {
                        echo '<li>'. $value . '</li>';
                      }
                    ?>
									</ul>
								</div><!-- end .info-content -->
							</div><!-- end .info-text -->
							<?php endif; ?>

						</div>
					</div><!-- end .col-md-6 col-sm-6 -->
				</div><!-- end .row -->

			</article>
			<!-- /article -->

		<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h2 class="title text-center"><?php _e('Sorry, nothing to display.', 'indohotels'); ?></h2>

		</article>
		<!-- /article -->

	<?php endif; ?>

</section>

<div class="row">
<?php if (!empty(rwmb_meta('near_location'))) : ?>
	<div class="col-md-6">
		<?php echo rwmb_meta('map'); ?>
	</div>
	<div class="col-md-6">
		<?php echo rwmb_meta('near_location'); ?>
	</div>
	<?php else: ?>
	<div class="col-md-12">
		<?php echo rwmb_meta('map'); ?>
	</div>
<?php endif; ?>
</div>

<!-- /section -->
</main>

<?php //get_sidebar();?>

<?php get_footer(); ?>
