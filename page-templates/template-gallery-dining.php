<?php /* Template Name: Dining Template */ get_header('image'); ?>

<main role="main">
	<!-- section -->
	<section class="container">
		<?php
		$args = array(
			'post_type'=>'gallery',
			'phototype'=>'dining',
		);
		query_posts($args);
		?>
		<h1 class="title text-center"><?php the_title(); ?></h1>
		<div class="box-gallery col-md-12">
			<div class="gallery-view">
				<!-- Slider -->
				<?php if (have_posts()):
					$i=0;
					while (have_posts()) : the_post();
				?>
				<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
					<div class="slider-item">
						<?php the_post_thumbnail('gallery-slide');?>
					</div>
				</div>
				<?php $i++; ?>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
			<?php the_excerpt(); ?>
			<?php the_content(); ?>
			<div class="gallery-nav">
			<!-- Slider -->
				<?php if (have_posts()):
					$i=0;
					while (have_posts()) : the_post();
				?>
				<div class="item <?php echo ($i==0)?'active':'' ?>" data-slide-number="<?php echo $i; ?>">
					<?php the_post_thumbnail('gallery-slide');?>
				</div>
				<?php $i++; ?>
				<?php endwhile; ?>
				<?php endif; ?>
			<!-- end slider -->
			</div>
		</div>
	</section>
	<!-- /section -->
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
