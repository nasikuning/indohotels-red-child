<?php /*
$mainColor = Main Color
$seColor = Secondary Color
$terColor = tertiary Color

== Change Body typography ==
$font_body =  $fontsColor['font-family']
$fontsColor['font-color'] = Body typography Color
$fontHover = Hover color text

== Heading Font ==
$fontHeading['font-family']

== Nav Stylish ==
 $navStyle['font-color']
 $navStyle['font-family']
 $navStyle['font-size']
 $navStyle['font-style']
 $navStyle['font-weight']

== Nav Background ==
 $navBg['background-color']
 $navBg['background-repeat']
 $navBg['background-attachment']
 $navBg['background-position']
 $navBg['background-size']
 $navBg['background-image']

== Main Background ==
 $mainBg['background-color']
 $mainBg['background-repeat']
 $mainBg['background-attachment']
 $mainBg['background-position']
 $mainBg['background-size']
 $mainBg['background-image']


== Footer Background ==
 $footerBg['background-color']
 $footerBg['background-repeat']
 $footerBg['background-attachment']
 $footerBg['background-position']
 $footerBg['background-size']
 $footerBg['background-image']

*/?>
<style id="indohotels-style" type="text/css">
	body {
		font-family: <?php echo isset($font_body) ? $font_body : ''; ?>;
		color: <?php echo isset($fontsColor['font-color']) ? $fontsColor['font-color'] : ''; ?>;
		background-color: <?php  $mainBg['background-color']; ?>;
	}

	::selection {
		background: <?php echo $mainColor; ?>;
	}

	p {
		font-size: <?php echo isset($fontsColor['font-size']) ? $fontsColor['font-size'] : '14px'; ?>;
	}

	.nav-background,
	.nav-no-image {
		background-color: <?php echo isset($navBg['background-color']) ? $navBg['background-color'] : ''; ?>;
		color: <?php echo isset($navStyle['font-color']) ? $navStyle['font-color'] : ''; ?>;
	}
	nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}
	.nav ul li a, .navbar-nav>li>a {
		color: <?php echo isset($navStyle['font-color']) ? $navStyle['font-color'] : ''; ?>;
		font-family: <?php echo isset($navStyle['font-family']) ? $navStyle['font-family'] : ''; ?>;
		text-transform: <?php echo isset($navStyle['text-transform']) ? $navStyle['text-transform'] : ''; ?>;
		font-weight: <?php echo isset($navStyle['font-weight']) ? $navStyle['font-weight'] : ''; ?>;
		font-size: <?php echo isset($navStyle['font-size']) ? $navStyle['font-size'] : ''; ?>;
	}

	button,
	html input[type=button],
	input[type=reset],
	input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}

	.popup-gmaps span {
		background: <?php echo $mainColor; ?>;
	}
	.popup-gmaps span:hover {
		background: <?php echo $seColor; ?>;
	}
	.popup-gmaps span i {
		color: #fff;
	}

	/* HEADING ========================================================== */
	h1.title {
		color: <?php echo $mainColor; ?>;
	}
	.heading-box h2,
	body.home h2 {
		font-family: <?php echo isset($fontHeading['font-family']) ? $fontHeading['font-family'] : ''; ?>;
		color: <?php echo isset($fontHeading['font-color']) ? $fontHeading['font-color'] : ''; ?>;
		text-transform: <?php echo isset($fontHeading['text-transform']) ? $fontHeading['text-transform'] : ''; ?>;
		font-weight: <?php echo isset($fontHeading['font-weight']) ? $fontHeading['font-weight'] : ''; ?>;
		font-size: <?php echo isset($fontHeading['font-size']) ? $fontHeading['font-size'] : ''; ?>;
	}

	/* LAYOUT ========================================================== */
	main[role="main"] {
		background-image: url(<?php echo $mainBg['background-image']; ?>);
		background-repeat: <?php echo $mainBg['background-repeat']; ?>;
		background-color: <?php echo $mainBg['background-color']; ?>;
	}

	@media (max-width: 1024px){
		.booking-box {
		  position: static;
		  max-width: 100%;
			background: <?php echo $mainColor; ?>;
		}
	}

	.booking .form-control,
	.booking .form-control[readonly],
	.booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;
	}

	/* PAGES ========================================================== */
	.header-image-page.plain {
		background: <?php echo $mainColor; ?>;
	}

	.inner-news .title-room-list a {
		color: <?php echo $mainColor; ?>;
	}
	.inner-news .title-room-list a:hover {
		color: <?php echo $mainColor; ?>;
		text-decoration: underline;
	}

	.info-text-alpha i {
		background: <?php echo $mainColor; ?>;
	}

	/* BUTTON ========================================================== */
	.btn-check,
	.btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}
	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}

	.btn-outline:hover,
	.btn-outline:focus,
	.btn-outline:active,
	.btn-outline.active{
		color: <?php echo $seColor; ?>;
	}

	.nav ul.dropdown-menu,
	.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline {
		background-color: <?php echo $mainColor; ?>
	}

	.slick-prev:before,
	.slick-next:before {
		color: <?php echo $mainColor; ?>
	}

	@media(max-width: 768px){
		.navbar .navbar-collapse {
	    background: <?php echo $mainColor; ?>;
		}
	}
	.info-text .info-icon .icon-round {
		background: <?php echo $seColor; ?>;
	}

	.book-room:hover,
	.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left,
	.owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $seColor; ?>;
	}

	#home-featured .box-room {
		background-color: <?php echo $mainColor; ?>;
	}
	#home-featured h2,
	.booking .booking-title {
		color:#fff;
	}

	.home-carousel {
		border-bottom: 1px solid <?php echo $mainColor; ?>;
	}

	.box-home-grid .overlay {
		background-color: <?php echo $terColor; ?>;
	}

	/* FACILITIES ========================================================== */
	.box-text h4 {
		color: <?php echo $seColor; ?>;
	}
	.box h4 {
		background-color: <?php echo $mainColor; ?>;
	}
	.box-home-grid {
		background-color: <?php echo $terColor; ?>;
	}

	/* HOMEPAGE TESTIMONIAL ========================================================== */
	.hotel-info-home {
		background-image: url(<?php echo $section3bg['background-image']; ?>);
		background-repeat: <?php echo $section3bg['background-repeat']; ?>;
		background-color: <?php echo $section3bg['background-color']; ?>;
	}

	/* ROOMS */
	.room-thumb:hover,.room-thumb:active {
		background-color: <?php echo $mainColor; ?>;
		color: #fff;
	}
	.room-details h3.room-details-f-title {
		color: <?php echo $seColor; ?>;
	}

	/* FOOTER ========================================================== */
	footer {
		clear: both;
		background-color: <?php echo isset($footerBg['background-color']) ? $footerBg['background-color'] : '#6d6e71'; ?>;
		background-image: url(<?php echo isset($footerBg['background-image']) ? $footerBg['background-image'] : ''; ?>);
		background-repeat: <?php echo isset($footerBg['background-repeat']) ? $footerBg['background-repeat'] : ''; ?>;
		border-top: 2px solid <?php echo isset($mainColor) ? $mainColor : '#000'; ?>;
	}
	.footer-distributed .footer-links li:hover a {
		color: <?php echo $fontHover; ?>;
		background: <?php echo $seColor; ?>;
	}

	.footer-distributed {
		background-color: <?php echo $mainColor; ?>;
	}
	.footer-distributed .krs_info_widget  p a {
		color: <?php echo $seColor; ?>;
	}
	.footer-distributed .krs_info_widget i {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a:hover {
		background-color: <?php echo $mainColor; ?>;
		color: #ffffff;
		border: 2px solid <?php echo $mainColor; ?>;
	}
	.footer-credits {
		background-color: <?php echo $terColor; ?>;
	}

	/* CALERAN ========================================================== */
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end {
		background-color: <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-separator {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-today,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-today {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-footer button.caleran-cancel {
		border: 1px solid <?php echo $seColor; ?> !important;
		color: <?php echo $seColor; ?> !important;
	}
	.caleran-apply-d,
	.caleran-apply {
		background: <?php echo $seColor; ?> !important;
		border: 1px solid <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick {
		background: <?php echo $seColor; ?>;
	}
</style>
